package com.venkatesh.springboot.thymeleaf.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.venkatesh.springboot.thymeleaf.model.Student;

@Controller
public class HelloController {
	
	@RequestMapping("/hello")
	public String hello() {
		return "HelloGuru";
		
	}
	
	@RequestMapping("/sendData")
	public ModelAndView sendData() {
		ModelAndView modelAndView=new ModelAndView("Data");
		modelAndView.addObject("message", "Take up one idea and make it your life");
		return modelAndView;
	}

	@RequestMapping("/student")
	public ModelAndView getStudent() {
		ModelAndView modelAndView = new ModelAndView("student");
		Student student = new Student();
		student.setName("Venkat");
		student.setScore(90);
		modelAndView.addObject("student",student);
		return modelAndView;
	}
	
	@RequestMapping("/studentForm")
	public ModelAndView getStudentForm() {
		ModelAndView View=new ModelAndView("StudentForm");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              		ModelAndView modelAndView = new ModelAndView("StudentForm");
    	Student student = new Student();
//		student.setName("Venkat");
//		student.setScore(90);
		View.addObject("child",student);
		return modelAndView;
	}
	
	@RequestMapping("/saveStudent")
	public ModelAndView saveStudentForm(@ModelAttribute Student student) {
		ModelAndView modelAndView = new ModelAndView("result");
		System.out.println("Name: "+student.getScore());
		return modelAndView;
	}
	
	@RequestMapping("/students")
	public ModelAndView getStudents() {
		ModelAndView modelAndView = new ModelAndView("Students");
		Student student = new Student();
		student.setName("Venkat");
		student.setScore(90);
		Student student2 = new Student();
		student2.setName("Bob");
		student2.setScore(90);
		List<Student> studentlist= Arrays.asList(student,student2);
		modelAndView.addObject("students",studentlist);
		return modelAndView;
	}
}
